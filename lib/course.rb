class Course
  attr_accessor :courses, :department, :credits, :students, :days, :time_block

  def initialize(courses, department, credits, days = nil, time_block = nil)
    @courses = courses
    @department = department
    @credits = credits
    @students = []
    @days = days
    @time_block = time_block
  end

  def name
    @courses
  end

  def add_student(student)
    student.enroll(self)
  end

  def conflicts_with?(course)
    (self.days & course.days).size > 0 && self.time_block == course.time_block
  end
end
