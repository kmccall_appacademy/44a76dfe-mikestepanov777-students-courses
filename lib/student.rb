class Student
  attr_accessor :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    @first_name + " " + @last_name
  end

  def enroll(course)
    conflicts = @courses.select{|crs| course.conflicts_with?(crs)}
    if conflicts.empty? && (!@courses.include?(course))
      @courses << course
      @courses.last.students << self
    elsif conflicts.size > 0
      raise "There is a course conflict with your #{course}!"
    end
  end

  def course_load
    hash = Hash.new(0)
    @courses.each do |course|
      hash[course.department] += course.credits
    end
    hash
  end
end
